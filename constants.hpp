#pragma once

#include "raylib-cpp.hpp"

inline const int WINDOW_WIDTH = 800;
inline const int WINDOW_HEIGHT = 450;
inline const raylib::Vector2 WINDOW_MIN_SIZE(320, 240);

inline const int SCREEN_WIDTH = 8000;
inline const int SCREEN_HEIGHT = 4500;

inline const float TIMELINE_HEIGHT = SCREEN_HEIGHT*0.8f;
inline const int TIMELINE_THICKNESS = 400;
inline const raylib::Vector2 TIMELINE_START(SCREEN_WIDTH*0.4f, SCREEN_HEIGHT);
inline const raylib::Vector2 TIMELINE_END(SCREEN_WIDTH*0.8f, SCREEN_HEIGHT);

