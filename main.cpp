#include "raylib-cpp.hpp"
#include "constants.hpp"
#include <cmath>
#include <iostream>
#include <cstdlib>

#if defined(PLATFORM_WEB)
    #include <emscripten/emscripten.h>

    EM_JS(int, canvasGetWidth, (), {
        return document.getElementById('canvas').clientWidth;
    });

    EM_JS(int, canvasGetHeight, (), {
        return document.getElementById('canvas').clientHeight;
    });
#endif

class Game {
    public:
        Game() :
            window(WINDOW_WIDTH, WINDOW_HEIGHT, "gmtk2023"),
            renderTarget(SCREEN_WIDTH, SCREEN_HEIGHT),
            targetTexture(renderTarget.texture),
            audioDevice(),
            mouse(),
            prevTime(GetTime())

        {
            window.SetMinSize(WINDOW_MIN_SIZE);
            window.SetState(FLAG_WINDOW_RESIZABLE | FLAG_VSYNC_HINT);

            targetTexture.SetFilter(TEXTURE_FILTER_BILINEAR);
        }

#if defined(PLATFORM_WEB)
        static void emCallback(void* arg) {
            try {
                auto game = static_cast<Game*>(arg);
                //resize the canvas
                static int oldWidth = 0;
                static int oldHeight = 0;

                int newWidth = canvasGetWidth();
                int newHeight = canvasGetHeight();
                if (newWidth != oldWidth || newHeight != oldHeight) {
                    game->windowSetSize(newWidth, newHeight);
                }

                //do a game update
                game->update();
            } catch (std::exception& e) {
                std::cerr << "Exception occurred: " << e.what() << std::endl;
                //std::cerr << "Exception occurred" << std::endl;
            } catch (...) {  //no exceptions may pass!
                std::cerr << "Unknown exception occurred" << std::endl;
            }
        }
#else
        //for desktop
        void run() {
            while (!window.ShouldClose()) {
                update();
            }
        }
#endif

    private:
        raylib::Window window;
        raylib::RenderTexture renderTarget;
        raylib::Texture targetTexture;
        raylib::AudioDevice audioDevice;
        raylib::Mouse mouse;
        double prevTime;

        void setWindowSize(int width, int height) {
            window.SetSize(width, height);
        }

        static float clamp(float value, float min, float max) {
            if (value < min) {
                return min;
            } else if (value > max) {
                return max;
            } else {
                return value;
            }
        }

        void update() {
            double currentTime = GetTime();
            double deltaTime = currentTime - prevTime;

            float scale = std::min(static_cast<float>(window.GetWidth()) / SCREEN_WIDTH,
                                   static_cast<float>(window.GetHeight()) / SCREEN_HEIGHT);

            //get the mouse coords
            raylib::Vector2 realMouse = mouse.GetPosition();
            float virtualMouseX = (realMouse.x - (window.GetWidth() - (SCREEN_WIDTH * scale)) * 0.5f) / scale;
            float virtualMouseY = (realMouse.y - (window.GetHeight() - (SCREEN_HEIGHT * scale)) * 0.5f) / scale;
            raylib::Vector2 virtualMouse(clamp(virtualMouseX, 0, SCREEN_WIDTH), clamp(virtualMouseY, 0, SCREEN_HEIGHT));

            // TODO game updates based on state

            // render the scene
            renderTarget.BeginMode();
            {
                raylib::Color::SkyBlue().ClearBackground();

                raylib::Color::Black().DrawLine(TIMELINE_START, TIMELINE_END, TIMELINE_THICKNESS);

            }
            renderTarget.EndMode();

            window.BeginDrawing();
            {
                raylib::Color::Gray().ClearBackground();

                // scale and draw the scene
                raylib::Rectangle sourceRec(0.0f,
                                            0.0f,
                                            static_cast<float>(targetTexture.width),
                                            static_cast<float>(-targetTexture.height));
                raylib::Rectangle destRec((window.GetWidth() - (SCREEN_WIDTH*scale))*0.5f,
                                          (window.GetHeight() - (SCREEN_WIDTH*scale))*0.5f,
                                          SCREEN_WIDTH*scale,
                                          SCREEN_HEIGHT*scale);
                targetTexture.Draw(sourceRec, destRec);
            }
            window.EndDrawing();
        }
};

int main() {
    Game game{};

#if defined(PLATFORM_WEB)
    emscripten_set_main_loop_arg(game.emCallback, &game, 0, 1);
#else
    game.run();
#endif

    return EXIT_SUCCESS;
}

