#pragma once

#include "raylib-cpp.hpp"
#include "constants.hpp"

enum CollisionType { solid, semisolid, none };

class Tile {
    public:
        const CollisionType collisionType;
        const bool removable;

        Tile(CollisionType collisionType, bool removable, std::shared_ptr<raylib::Texture> textureAtlas) :
            collisionType(collisionType),
            removable(removable),
            sprite(textureAtlas, raylib::Rectangle(collisionType * 100, removable, 100, 100))
        {}

        public void draw() {
            
        }

    private:
        

};

